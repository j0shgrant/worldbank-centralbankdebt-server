proto:
	protoc --go_out=. --go_opt=paths=source_relative \
            --go-grpc_out=. --go-grpc_opt=paths=source_relative \
            api/api.proto

build:
	go build -o . ./...

docker-build:
	docker build -t "gitlab.com/j0shgrant/worldbank-centralbankdebt-server:latest" .

clean:
	go clean