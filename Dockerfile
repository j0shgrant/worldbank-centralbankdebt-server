FROM golang:alpine AS build

WORKDIR /build
COPY . .
RUN go build -o . ./...

FROM alpine
COPY --from=build /build/debt-server /

ENTRYPOINT [ "/debt-server" ]
