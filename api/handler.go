package api

import (
	"context"
	"log"
)

type Server struct{}

func (s *Server) mustEmbedUnimplementedDebtServer() {
	panic("implement me")
}

func (s *Server) PutDebt(ctx context.Context, in *Record) (*PutStatus, error) {
	log.Println(in)
	return &PutStatus{Success: true}, nil
}
