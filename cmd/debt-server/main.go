package main

import (
	"gitlab.com/j0shgrant/worldbank-centralbankdebt-server/api"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	listener, err := net.Listen("tcp", ":7777")
	if err != nil {
		log.Fatal(err)
	}

	s := api.Server{}
	grpcServer := grpc.NewServer()
	api.RegisterDebtServer(grpcServer, &s)

	if err := grpcServer.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
